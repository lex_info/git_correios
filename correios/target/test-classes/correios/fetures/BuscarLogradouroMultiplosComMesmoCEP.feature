@BuscaCEP
Feature: A Busca CEP - Endereço deve retornar multiplos logradouro com um unico CEP valido 

Scenario Outline: Digitar nome logradouro valido na busca 

	Given que eu esteja no site do correio 
	When no campo busca eu digito o "<LOGRADOURO>" 
	Then retornara os dados dos multiplos logradouros 
	
	Examples: 
		| LOGRADOURO            |
		| Rua Santos Reis - até |