@BuscaCEPsimples
Feature: A Busca CEP - Endereço deve retornar um logradouro quando for digitado um CEP valido 

Scenario Outline: Digitar CEP valido na busca 

	Given que eu esteja no site dos correios 
	When no campo busca eu digito o "<CEP>" valido 
	Then retornara os dados do logradouro 
	
	Examples: 
		| CEP                   |
		| 04859090              |
		| Rua Santos Reis - até |
		