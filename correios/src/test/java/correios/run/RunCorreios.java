package correios.run;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = "src\\test\\java\\correios\\fetures\\",tags="@BuscaCEPsimples",
		glue= {""}, monochrome = true, dryRun = false
		)
public class RunCorreios {

}
