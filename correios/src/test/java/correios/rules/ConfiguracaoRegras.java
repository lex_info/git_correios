package correios.rules;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import static correios.core.DriverFactory.getDriver;
import static correios.core.DriverFactory.killDriver;

public class ConfiguracaoRegras {
	
	
	
	@Before
	public void abrirNavegador() 
	   {
		String caminho = System.getProperty("user.dir");
		System.setProperty("webdriver.chrome.driver", caminho+"/src/test/resources/driver/chromedriver.exe");
		getDriver();
		getDriver().navigate().to("http://www.buscacep.correios.com.br/sistemas/buscacep/buscaCepEndereco.cfm");		
		}
	
	
	@After
	public void fecharNavegador() 
	{
		killDriver();
	}
}
