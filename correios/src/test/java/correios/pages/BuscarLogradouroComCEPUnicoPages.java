package correios.pages;

import correios.core.SeleniumBase;

public class BuscarLogradouroComCEPUnicoPages extends SeleniumBase {
	
	
	public String  escreverCEPnaBusca(String texto) 
	{
		escrever("//input[@name='relaxation']", texto);
		clicarBotao("//input[@class='btn2 float-right']");
		return obterValorCampo("//h3[contains(text(),'Busca CEP - Endereço')]");
	}
	
	public String recuperarTodosOsValoresDaTabelaLogradouro() 
	{   
		recuperarDadosDaTabela();		
		return obterValorCampo("//p[contains(text(),'DADOS ENCONTRADOS COM SUCESSO.')]");
		
	}

}
