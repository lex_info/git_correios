package correios.core;

import static correios.core.DriverFactory.getDriver;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class SeleniumBase {
	
/********* TextField e TextArea ************/
	
	public void escrever(By by, String texto)
	{
		getDriver().findElement(by).clear();
		getDriver().findElement(by).sendKeys(texto);
	}

	public void escrever(String class_campo, String texto)
	{
		escrever(By.xpath(class_campo), texto);
	}
	
	public String obterValorCampo(String id_campo) 
	{
		return getDriver().findElement(By.xpath(id_campo)).getText();
	}
/********* Botao ************/
	
	public void clicarBotao(String id) 
	{
		getDriver().findElement(By.xpath(id)).click();
	}
/******* Tabelas *************************/
	
	public void recuperarDadosDaTabela() 
	{
	int x = 1;	
	for (int i = 2; i < contaLinhaTabela(); i++) {
	
      System.out.println("-------------- Registro:"+x+"---------------------\n");
		for (int j = 1; j < 5; j++) {
			String valueXpath = "//table/tbody/tr["+i+"]/td["+j+"]";
			WebElement element = getDriver().findElement(By.xpath(valueXpath));
			String elementValue = element.getText().trim();
			System.out.println("Valor tabela: "+elementValue);
		}	
		x=x+1;
	}
  }
	
	public int contaLinhaTabela() 
	{
		int rowCount = getDriver().findElements(By.xpath("//table[@class='tmptabela']/tbody/tr")).size();		
		return rowCount + 1;
		
	}
}
