package correios.steps;

import static correios.core.DriverFactory.getDriver;
import static org.junit.Assert.assertEquals;

import correios.pages.BuscarLogradouroComCEPUnicoPages;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class BuscarLogradouroComCEPUnicoSteps extends BuscarLogradouroComCEPUnicoPages {
	
	@Given("^que eu esteja no site dos correios$")
	public void que_eu_esteja_no_site_dos() throws Throwable {	   
	    try {	    	
			assertEquals("buscaCepEndereço", getDriver().getTitle());
		} catch (Exception e) {
			System.out.println("Falha ao recuperar o texto do titulo da pagina.");
		}
	}

	@When("^no campo busca eu digito o \"(.*?)\" valido$")
	public void no_campo_busca_eu_digito_o_valido(String cep) throws Throwable {		
		try {	    	
			assertEquals("Busca CEP - Endereço", escreverCEPnaBusca(cep));
		} catch (Exception e) {
			System.out.println("Falha ao recuperar o texto de validação pagina Busca CEP");
		}			
	}

	@Then("^retornara os dados do logradouro$")
	public void retornara_os_dados_do_logradouro() throws Throwable {
		try {
			assertEquals("DADOS ENCONTRADOS COM SUCESSO.", recuperarTodosOsValoresDaTabelaLogradouro() );
		} catch (Exception e) {
			System.out.println("Nenhum valor foi localizado na tabela CEP");
		}
	}

}
